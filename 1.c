#include<stdio.h>
int main()
{
    //This is a program to find the relationship between profit and ticket price

    int x,y,a,ex,pro,in;
    //Enter the ticket price to find profit
    printf("Enter ticket price\n");
    scanf("%d",&x);

    if(x==15)
    {a=120;}
    else if(x>15)
    {
        y=x-15;
        a=120-(y*4);
    }
    else
    {
        y=15-x;
        a=120+(y*4);
    }

    //Formula to find, Income= Ticket price * Attendance
    in=(x*a);
    printf("Income=%d\n",in);
    //Formula to find, Expend= 500 + (3 * Attendance)
    ex=500+(3*a);
    printf("Expend= %d\n",ex);
    //Formula to find, Profit= Income - Expend
    pro=in-ex;
    printf("Profit=%d\n",pro);

    return 0;

}
